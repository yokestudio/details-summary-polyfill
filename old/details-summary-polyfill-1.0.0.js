﻿/***
 * Limitations:
 * 1) Future elements will only work for browsers supporting MutationObserver (IE11+)
 * 
 * Other Side-Effects:
 * 1) tabIndex on all <summary>
 * 
 ***/
(function(global) {
	"use strict";

	// Check if we need this polyfill
	if ('open' in document.createElement('details')) {
		return;
	}

	// Init when DOM is loaded
	//console.log('DEBUG polyfilling details');
	if (document.readyState !== "complete") {
		document.addEventListener("DOMContentLoaded", init, false);
	}
	else {
		init();
	}

	function init() {
		var i,len; // loop vars

		// Add in light default styles
		document.head.insertAdjacentHTML('afterbegin', '<br><style>' + //<br> need for all IE
			'details,summary{display: block;}'+
			'details>summary:focus{outline:1px solid blue;}'+
			'details>summary::before{content:"►";margin-right: 5px;}'+
			'details[open]>summary::before{content:"▼";}'+
		"</style>");

		// Make sure "open" attribute is gettable and settable
		Object.defineProperty(global['Element'].prototype, 'open', {
			"get" : function() {
				if (!('nodeName' in this) || this.nodeName.toUpperCase() != 'DETAILS') {
					return void 0;
				}
				
				return this.hasAttribute('open');
			},
			"set" : function(val) {
				if (!('nodeName' in this) || this.nodeName.toUpperCase() != 'DETAILS') {
					return void 0;
				}
				
				if (val) {
					this.setAttribute('open', 'open');
				}
				else {
					this.removeAttribute('open');
				}
			}
		});

		// Grab all existing details / summary on the page
		var dom_detailses = document.getElementsByTagName('details');
		var dom_summaries = document.getElementsByTagName('summary');		

		// Init open state for all <details>
		for (i=0, len=dom_detailses.length; i<len; i++) {
			prepareDetails(dom_detailses[i]);
		}

		// Init tabIndex for all <summary>
		for (i=0, len=dom_summaries.length; i<len; i++) {
			prepareSummary(dom_summaries[i]);
		}

		// Set up Mutation Observer to observe future elements
		if (global.MutationObserver) {
			var observer = new MutationObserver(function(mutations) {
				mutations.forEach(function(mutation) {
					// look through all added nodes of this mutation
        			for (var j=0, len=mutation.addedNodes.length; j<len; j++) {
        				var node_name = mutation.addedNodes[j].nodeName.toUpperCase();
        				if (node_name === 'DETAILS') {
        					prepareDetails(mutation.addedNodes[j]);
        				}
        			} // for each added node
				});
			});
			observer.observe(document.body, {childList: true, subtree: true}); // observe subtree for changes
		}
		// Bindings on body
		document.body.addEventListener('click', onBodyClick, true);
		document.body.addEventListener('keydown', onBodyKeydown, true);

		// Clean Up
		document.removeEventListener("DOMContentLoaded", init, false);
	} //init()

	/**
	 * Gets the parent <details> element from a node wihin the <summary>.
	 * @param: {HTMLElement}
	 * @return: {HTMLElement}
	 */
	function getParentDetailFromSummary(tgt) {
		var dom_body = document.body;
		while (tgt !== dom_body) {
			if (tgt.nodeName.toUpperCase() === 'SUMMARY') {
				return tgt.parentElement;
			}
			tgt = tgt.parentElement;
		}

		return null;
	}

	function prepareDetails(dom_details) {
		// Find first summary tag
		var dom_summary;
		for (var i=0,len = dom_details.children.length; i<len; i++) {
			var node_name = dom_details.children[i].nodeName.toUpperCase();
			if (node_name === 'SUMMARY') {
				dom_summary = dom_details.children[i];
				break;
			}
		}

		// Auto create summary if not found
		if (!dom_summary) {
			//console.log('DEBUG auto-create summary');
			dom_summary = document.createElement('summary');
			dom_summary.innerHTML = 'Details';
		}

		// Ensure summary is first child
		dom_details.insertBefore(dom_summary, dom_details.firstChild);

		prepareSummary(dom_summary);

		activateDetailsOpenState(dom_details);
	}

	function prepareSummary(dom_summary) {
		// Add tabIndex to make it tab-able
		dom_summary['tabIndex'] = '0';
	}

	function toggleDetailsOpenState(dom_details) {
		if (dom_details.hasAttribute('open')) {
			dom_details.removeAttribute('open');	
		}
		else {
			dom_details.setAttribute('open', 'open');
		}

		activateDetailsOpenState(dom_details);

		// Dispatch toggle event
		var event = new Event('toggle');
		dom_details.dispatchEvent(event);
	}

	function activateDetailsOpenState(dom_details) {
		var i, len, dom_el; //loop vars

		if (dom_details.hasAttribute('open')) {
			for (i=1,len=dom_details.children.length; i<len; i++) {
				dom_el = dom_details.children[i];

				// Ignore summary
				/*if (dom_el.nodeName.toUpperCase() === 'SUMMARY') {
					continue;
				}
				*/
				
				// Set back to original display attribute
				var orig_display = dom_el.getAttribute('data-details-open-display');
				if (orig_display) {
					dom_el.style['display'] = orig_display;
				}
				else {
					dom_el.style['display'] = '';
				}
			}
		}
		else {
			for (i=1,len=dom_details.children.length; i<len; i++) {
				dom_el = dom_details.children[i];

				// Ignore summary
				/*
				if (dom_el.nodeName.toUpperCase() === 'SUMMARY') {
					continue;
				}
				*/

				// Set display to none, storing original value if necessary
				if (dom_el.style['display']) {
					dom_el.setAttribute('data-details-open-display', dom_el.style['display']);
				}
				dom_el.style['display'] = 'none';
			}
		}
	} //activateDetailsOpenState()

	/*************
	 * Listeners *
	 *************/
	function onBodyKeydown(e) {
		var tgt = e.target;

		var dom_details = getParentDetailFromSummary(tgt);
		if (dom_details !== null) {
			if (e.keyCode === 13 || e.keyCode === 32) {
				e.preventDefault();
				toggleDetailsOpenState(dom_details);
			}
		}
	} //onBodyKeydown()

	function onBodyClick(e) {
		var tgt = e.target;

		var dom_details = getParentDetailFromSummary(tgt);
		if (dom_details !== null) {
			toggleDetailsOpenState(dom_details);
		}
	} //onBodyClick()
})(this);
