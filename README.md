# HTML Details / Summary Polyfill #

A JavaScript polyfill for `<details>` and `<summary>`.

This polyfill works by listening for any `<details>` element added to the DOM, and manipulating its children to display according to its open/closed state in real-time. This ensures that #ALL# `<details>` elements, even future ones dynamically injected through JS, will behave correctly. 

### Browser Support ###
* IE 11+ (IE 9 / 10 support only static elements)
* Edge
* Firefox

As far as we know, Webkit/Blink browsers already natively support `<details>` and hence do not need this polyfill. Including the polyfill won't harm or affect these browsers in any way, though.

### Set Up ###

1. Download the latest release (as of 11 Feb 2016, v1.0.1).
2. Include in your HTML (preferably just before `</body>`):

    `<script src="details-summary-polyfill-1.0.1.js"></script>`

### API ###

Refer to MDN [details](https://developer.mozilla.org/en/docs/Web/HTML/Element/details) and [summary](https://developer.mozilla.org/en/docs/Web/HTML/Element/summary).

### FAQ ###

1. Does this polyfill have any bugs/limitations?
>  - IE browsers below version 11 will not work with dynamically injected elements (e.g. AJAX DOM insertions). Because we try to adhere to web standards and don't really want to use deprecated technology, we use `MutationObserver` to listen for future elements. Check out [Can I Use](http://caniuse.com/#feat=mutationobserver) for more detailed browser support. Of course, if you apply a `MutationObserver` polyfill before this polyfill, you may also be able to get this polyfill to work with legacy browsers older than IE11. We generally don't want to bother ourselves with these legacy browsers, so we have never tested it out on IE 10 and below. If you have tried it and it works, let us know.
>
>  - In the closed state, the children elements of details will be forced to `display:none` (inline style attribute). If any of the children elements has `!important` suffix on the display property in your CSS, it would override the polyfill's behaviour. While we could have implemented a more complicated solution involving overriding CSS Text, we didn't think it was worth it as developers are strongly discouraged from using `!important` in their code.
>
>  - Upon a successful open/close action, a `toggle` event is triggered on the `<details>` element, as per the specification. However, as we are generating these events manually in the polyfill, these toggle events have the attribute `isTrusted: false`.
>

2. Are there any side-effects?
>  - All `<summary>` tags will be set to `tabIndex:0`. This ensures that the summary element is accessible via keyboard tabs, like it should.
>
>  - Because we are listening out for DOM changes, performance MAY be an issue on older/slower/low-powered devices. Thankfully, a large majority of popular mobile devices are running webkit/blink browsers, which have native support for `<details>`, so this polyfill does nothing for them.

3. How do we style the details marker (triangle)?
>  - The CSS pseudo-element `summary::-webkit-details-marker` only works for webkit browsers (obviously). For the other browsers (Firefox / IE), we used Unicode triangle characters on the `summary::before` pseudo-element. This should work pretty well and produce a basic-looking details marker that looks almost like the webkit one as a default. However, we encourage you to style the details markers yourself using CSS like so:
>

```
summary::-webkit-details-marker {display: none;} /* remove default webkit style */
details > summary::before {
    content:'+';  /* or whatever character, or empty and use a background-image instead */
    float: left;  /* take it outside of document flow */
	margin-right: 5px; 
	font-family: "Lucida Sans Unicode","Lucida Grande",sans-serif; /* try to use a unicode font */
}
```



### Contact ###

* Email us at <yokestudio@hotmail.com>.